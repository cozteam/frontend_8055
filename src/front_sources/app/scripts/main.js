$(document).ready(function() {
  setTimeout(function(){
    $('body').addClass('__visible');
  },500);
  var overlay = $('.j-overlay'),
    formsHolder = $('.j-modals'),
    modalCloseBtn = $('.j-modal__close'),
    heroBtn = $('.j-hero__modal-open'),
    heroModal = $('.j-hero__modal'),
    successModal = $('.j-success-modal');
  
  function showOverlay(overlay) {
    overlay.fadeIn();
  }
  function hideOverlay(overlay) {
    overlay.fadeOut();
  }

  function hideModals() {
    heroModal.fadeOut();
    successModal.fadeOut();
    setTimeout(function(){
      formsHolder.fadeOut();
      hideOverlay(overlay);
    },400);
  }

  modalCloseBtn.click(function() {
    hideModals();
  });
  formsHolder.click(function() {
    hideModals();
  });
  heroModal.click(function(e) {
    e.stopPropagation();
  });
  successModal.click(function(e) {
    e.stopPropagation();
  });

  heroBtn.click(function() {
    var heroBtnItem = $(this),
        heroBtnItemData = heroBtnItem.data('rooms');
    showOverlay(overlay);
    formsHolder.fadeIn();
    heroModal.each(function(){
      var heroModalItem = $(this),
          heroModalItemData = heroModalItem.data('rooms');
      if(heroBtnItemData === heroModalItemData) {
        setTimeout(function(){
          heroModalItem.fadeIn();
        },400);
      }
    });
    
  });

  $('.j-scrollto').click(function(){
    var link = $(this),
        target = link.attr('href'),
        targetOffset = $(target).offset().top;
    return $('html:not(:animated),body:not(:animated)').animate({
      scrollTop: targetOffset
    }, 700), !1
  });

  /* select js */
  
  $(document).on('click', '.j-select', function() {
    var selectedField = $(this).parent(),
        selectedOptions = selectedField.find('.j-select-options');
        selectedField.toggleClass('__opened');
        selectedOptions.toggleClass('__active');
  });

  $(document).on('click', '.j-field-option', function() {
    var selectedOption = $(this),
        selectedOptionVal = selectedOption.data('value'),
        selectedOptionData = selectedOption.data('count'),
        selectedOptionField = selectedOption.closest('.j-input-field__list').find('.j-select-input'),
        selectedOptionInput = selectedOption.closest('.j-input-field__list').find('.j-counter-input');
    selectedOptionField.val(selectedOptionVal);
    selectedOptionInput.val(selectedOptionData);
    console.log(selectedOptionInput.val());
    selectedOptionField.attr('data-count', selectedOptionData);
    selectedOption.closest('.j-input-field__list').removeClass('__opened');
    selectedOption.parent().removeClass('__active');
    selectedOption.parent().find('.j-field-option').removeClass('__selected');
    selectedOption.addClass('__selected');
    setTimeout(function(){
      getEstimate();
    },200);
  });

  $(document).on('click', '.j-allergy-option', function() {
    setTimeout(function(){
      getEstimate();
    },200);
  });

  var calcForm = $('.j-calculator-form');

  /* get phone and links */

  function getCallbackLinks() {
    $.ajax({
      url: 'https://8055.ru/api/contacts',
      method: 'get',
      dataType: 'json',
      success: function(data){
        setCallbackLinks(data);
      }
    });
  }
  getCallbackLinks();

  function setCallbackLinks(data) {
    for (var key in data) {
      var callbackLinkName = key,
          callbackLink = data[key];
      // console.log(callbackLinkName);
      // console.log(callbackLink);
      if(callbackLinkName === 'phone') {
        var callbackPhoneBLock = $('.j-callback-phone');
        callbackPhoneBLock.each(function(){
          var phoneBLockItem = $(this),
          phoneBLockItemHref = 'tel:'+callbackLink;
          phoneBLockItem.text(callbackLink);
          phoneBLockItem.attr('href', phoneBLockItemHref);
        });
      } else if (callbackLinkName === 'telegram') {
        var callbackTgBLock = $('.j-callback-tg');
        callbackTgBLock.each(function(){
          var callbackTgBLockItem = $(this);
          callbackTgBLockItem.attr('href', callbackLink);
        });
      } else if (callbackLinkName === 'whatsapp') {
        var callbackWtBLock = $('.j-callback-whatsapp');
        callbackWtBLock.each(function(){
          var callbackWtBLockItem = $(this);
          callbackWtBLockItem.attr('href', callbackLink);
        });
      } else if (callbackLinkName === 'viber') {
        var callbackVbBLock = $('.j-callback-viber');
        callbackVbBLock.each(function(){
          var callbackVbBLockItem = $(this);
          callbackVbBLockItem.attr('href', callbackLink);
        });
      }
    }
  }

  /* get phone and links end */

  function setPrices(data) {
    for (var key in data) {
      var priceItemSum = data[key],
          priceItemName = key;

      if(priceItemName === 'Однушка') {
        $('.j-1k-flat-price').text(priceItemSum);
      } else if(priceItemName === 'Двушка') {
        $('.j-2k-flat-price').text(priceItemSum);
      } else if(priceItemName === 'Трешка') {
        $('.j-3k-flat-price').text(priceItemSum);
      } else if(priceItemName === 'Дом') {
        $('.j-house-flat-price').text(priceItemSum);
      } else if(priceItemName === 'Офис') {
        $('.j-office-flat-price').text(priceItemSum);
      }
    }
  }

  function getPrices() {
    $.ajax({
      url: 'https://8055.ru/api/price',
      method: 'get',
      dataType: 'json',
      success: function(data){
        setPrices(data);
      }
    });
  }
  getPrices();

  function setFlatsTypes(data) {
    var flatTypeInput = $('.j-flat-type-property'),
        flatTypeDefaultInput = $('.j-flat-type-property-default'),
        flatTypeList = $('.j-flat-type-list');

    // console.log(data);
    for (var key in data) {
      var flatSelect = data[key],
          flatSelectKey = key,
          flatSelectTitle = flatSelect.title,
          flatSelectActive = flatSelect.active;
      // console.log('flatSelect: ' + flatSelect);
      // console.log('flatSelectKey: ' + flatSelectKey);
      // console.log('flatSelectTitle: ' + flatSelectTitle);
      // console.log('flatSelectActive: ' + flatSelectActive);
      
      if(flatSelectActive === 1) {
        flatTypeInput.prepend('<label for="property">Тип помещения</label>');
        flatTypeDefaultInput.prepend('<div class="j-select"><input class="j-select-input j-property" id="property" type="text" readonly="" data-count='+flatSelectKey+' placeholder="Выбрать" value='+ flatSelectTitle +'></div><input type="text" class="__hidden j-counter-input" value='+flatSelectKey+'>')
        flatTypeList.append('<span class="input-field__option-item __selected j-field-option" data-value='+ flatSelectTitle +' data-count='+flatSelectKey+'>'+ flatSelectTitle +'</span>');
      }
      else {
        flatTypeList.append('<span class="input-field__option-item j-field-option" data-value='+ flatSelectTitle +' data-count='+flatSelectKey+'>'+ flatSelectTitle +'</span>');
      }
    }
  }

  /* получение значений в селекты */
  function getFlatsTypes() {
    $.ajax({
      url: 'https://8055.ru/api/dropdown/property',
      method: 'get',
      dataType: 'json',
      success: function(data){
        setFlatsTypes(data);
      }
    });
  }
  getFlatsTypes();

  function setCleaningSelect(data) {
    var cleanTypeInput = $('.j-clean-type-property'),
        cleanTypeDefaultInput = $('.j-clean-type-property-default'),
        cleanTypeList = $('.j-clean-type-list');

    // console.log(data);
    for (var key in data) {
      var cleanSelect = data[key],
          cleanSelectKey = key,
          cleanSelectTitle = cleanSelect.title,
          cleanSelectActive = cleanSelect.active;
      // console.log('cleanSelect: ' + cleanSelect)
      // console.log('cleanSelectKey: ' + cleanSelectKey);
      // console.log('cleanSelectTitle: ' + cleanSelectTitle);
      // console.log('cleanSelectActive: ' + cleanSelectActive);
      
      if(cleanSelectActive === 1) {
        cleanTypeInput.prepend('<label for="cleaning">Вид уборки</label>');
        cleanTypeDefaultInput.prepend('<div class="j-select"><input class="j-select-input j-cleaning" id="cleaning" type="text" readonly="" data-count='+cleanSelectKey+' placeholder="Выбрать" value='+ cleanSelectTitle +'><input type="text" class="__hidden j-counter-input" value='+cleanSelectKey+'></div>')
        cleanTypeList.append('<span class="input-field__option-item __selected j-field-option" data-value='+ cleanSelectTitle +' data-count='+cleanSelectKey+'>'+ cleanSelectTitle +'</span>');
      }
      else {
        cleanTypeList.append('<span class="input-field__option-item j-field-option" data-value='+ cleanSelectTitle +' data-count='+cleanSelectKey+'>'+ cleanSelectTitle +'</span>');
      }
    }
  }

  function getCleaningSelect() {
    $.ajax({
      url: 'https://8055.ru/api/dropdown/service',
      method: 'get',
      dataType: 'json',
      success: function(data){
        setCleaningSelect(data);
      }
    });
  }
  getCleaningSelect();

  /* сбор данных с полей с чекбоксами */

  var servicesList = document.getElementById('calc-fields');
  
  function setServices(data) {
    for (var item in data) {
      var calcItemContent = document.createElement('div');
          calcItemContent.className = "calculator__bottom-part";
      
      var servicesListHeading = item,
          servicesListContent = data[item];
      // console.log('servicesListHeading: название категории')
      // console.log(servicesListHeading); // мойка окон
      calcItemContent.innerHTML += `<h3 class="calculator__bottom-heading j-calculator__fields-name">${servicesListHeading}</h3>`;
      // console.log('servicesListContent: список услуг категории');
      // console.log(servicesListContent); // список услуг категории

      for (var item in servicesListContent) {
        var serviceItemId = item,
            serviceItem = servicesListContent[item],
            serviceItemTitle = serviceItem.title,
            serviceItemActive = serviceItem.active,
            serviceItemAmount = serviceItem.amount;
        // console.log('--- услуга начало ---');
        // console.log('id услуги: ' + item);
        // console.log('serviceItem: одна услуга');
        // console.log(serviceItem); // одна услуга
        // console.log('serviceItemTitle: название услуги');
        // console.log(serviceItemTitle); // название услуги
        // console.log('serviceItemActive: активность услуги 1/0');
        // console.log(serviceItemActive); // активность услуги 1/0
        // console.log('serviceItemAmount: количество услуги ');
        // console.log(serviceItemAmount);
        // console.log('--- услуга конец ---');
        
        if(serviceItemActive === 0) {
          calcItemContent.innerHTML += `
            <div class="checkbox-option j-checkbox-option">
              <label class="checkbox__label-box" for="${serviceItemId}">
                <input class="checkbox__input j-checkbox__input" type="checkbox" id="${serviceItemId}">
                <div class="checkbox__label"></div>
              </label>
              <div class="checkbox__text-box">
                <label class="checkbox__text" for="${serviceItemId}">${serviceItemTitle}</label>
              </div>
              <div class="checkbox__counter-box">
                <input class="checkbox__counter j-checkbox__counter" type="text" placeholder="..." maxlength="4">
              </div>
            </div>
          `;
        } else {
          calcItemContent.innerHTML += `
            <div class="checkbox-option j-checkbox-option">
              <label class="checkbox__label-box" for="${serviceItemId}">
                <input class="checkbox__input j-checkbox__input" type="checkbox" checked="checked" id="${serviceItemId}">
                <div class="checkbox__label"></div>
              </label>
              <div class="checkbox__text-box">
                <label class="checkbox__text" for="${serviceItemId}">${serviceItemTitle}</label>
              </div>
              <div class="checkbox__counter-box">
                <input class="checkbox__counter j-checkbox__counter" type="text" placeholder="..." maxlength="4" value="${serviceItemAmount}">
              </div>
            </div>
          `;
        }
        servicesList.appendChild(calcItemContent);
      }
    }
    setTimeout(function(){
      getEstimate();
    },400);
  }

  function getServices() {
    $.ajax({
      url: 'https://8055.ru/api/services',
      method: 'get',
      dataType: 'json',
      data: {},
      success: function(data){
        setServices(data);
      }
    });
  }
  getServices();

  function getEstimate() {
    var checkboxFieldsList = $('.j-calculator-form').find('.j-checkbox__input'),
        servicesArr = [],
        propertyCounter = $(".j-calculator-form").find('.j-property'),
        propertyCounterInput = propertyCounter.closest('.j-input-field__list').find('.j-counter-input'),
        propertyCounterInputValue = Number(propertyCounterInput.val()),
        cleaningCounter = $(".j-calculator-form").find('.j-cleaning'),
        cleaningCounterInput = cleaningCounter.closest('.j-input-field__list').find('.j-counter-input'),
        cleaningCounterInputValue = Number(cleaningCounterInput.val()),
        allergyCounter = $('.j-calculator-form').find('.j-allergy-check').is(':checked');

    checkboxFieldsList.each(function(){
      var checkboxFieldItem = $(this),
          checkboxFieldItemId = checkboxFieldItem.attr('id'),
          checkboxFieldItemChecked = checkboxFieldItem.is(':checked');
          
      if(checkboxFieldItemChecked === true) {
        var checkboxFieldItemCounter = checkboxFieldItem.closest('.j-checkbox-option').find('.j-checkbox__counter'),
        checkboxFieldItemCounterVal = checkboxFieldItemCounter.val();
        
        if(checkboxFieldItemCounterVal.length > 0) {
          servicesArr.push({
            itemId: checkboxFieldItemId,
            itemValue: checkboxFieldItemCounterVal
          });
        }
      }
    });
   
    var resultServicesArr = servicesArr;
    // console.log(resultServicesArr);

    $.ajax({
      url: 'https://8055.ru/api/estimate',
      method: 'post',
      dataType: 'json',
      data: JSON.stringify({
        "services": resultServicesArr,
        "property": propertyCounterInputValue,
        "service": cleaningCounterInputValue,
        "is_allergy": allergyCounter
      }),
      success: function(data) {
        // console.log(data);
        $('.j-calculator__total-sum').text(data.price);
      }
    });
  }  

  /* ввод только цифр в поля */

  $(document).on('keydown', '.j-checkbox__counter', function(event) {
    if (event.keyCode == 107 || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
      (event.keyCode == 65 && event.ctrlKey === true) ||
      (event.keyCode == 109) ||        
      (event.keyCode >= 35 && event.keyCode <= 39)) {
      return;
    } else {
      if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
        event.preventDefault();
      }
    }
  });

  /* связи чекбоксов с полями для ввода цифр */
  
  $(document).on('input', '.j-checkbox__input', function() {
    var checkboxItem = $(this),
        checkboxItemChecked = checkboxItem.is(':checked'),
        checkboxItemCounter = checkboxItem.closest('.j-checkbox-option').find('.j-checkbox__counter'),
        checkboxItemCounterVal = Number(checkboxItemCounter.val());

    if(checkboxItemChecked === true) {
      if(checkboxItemCounterVal < 1) {
        checkboxItemCounter.val('1');
      }
    } else {
      checkboxItemCounter.val('');
    }
    setTimeout(function(){
      getEstimate();
    },200);
  });

  $(document).on('input', '.j-checkbox__counter', function() {
    var checkboxCounter = $(this), 
        checkboxCounterValue = Number(checkboxCounter.val());

    if(checkboxCounterValue > 0) {
      checkboxCounter.closest('.j-checkbox-option').find('.j-checkbox__input').prop('checked', true);
      checkboxCounter.removeClass('__to-fill');
    } else {
      checkboxCounter.closest('.j-checkbox-option').find('.j-checkbox__input').prop('checked', false);
    }
    setTimeout(function(){
      getEstimate();
    },200);
  });

  

  /* check callback form fields */
  var userNameField = $('.j-user-name-field');
  userNameField.keyup(function() {
    var userNameField = $(this),
    userNameLength = userNameField.val().length;
    // console.log(userNameLength);
    if(userNameLength < 2) {
      userNameField.addClass('__error');
    } else {
      userNameField.removeClass('__error');
    }
  });

  var userPhone = $('.j-user-phone');
  userPhone.keyup(function(){
    if (userPhone.val().length < 3) {
      userPhone.val('+7(');
    }
    if (userPhone.val().length == 15) {
      userPhone.removeClass('__error');
    }
  });

  userPhone.keydown(function(e) {
    var key = e.charCode || e.keyCode || 0;
    
    if (key !== 8 && key !== 9) {
      if (userPhone.val().length === 6) {
          userPhone.val(userPhone.val() + ')');
      }
      else if (userPhone.val().length === 7) {
        userPhone.val(userPhone.val() + ' ');
      }
      else if (userPhone.val().length === 10) {
        userPhone.val(userPhone.val() + '-');
      }
      else if (userPhone.val().length >= 10) {
        userPhone.val(userPhone.val().substring(0, 14));
      }
      else if (userPhone.val().length < 15) {
        userPhone.addClass('__error');
      } else {
        userPhone.removeClass('__error');
      }
    }
    // Allow numeric (and tab, backspace, delete) keys only
    return key == 8 || key == 9 || key == 46 || key >= 48 && key <= 57 || key >= 96 && key <= 105;
  });

  $('.j-user-address').keyup(function() {
    var userNameField = $(this),
    userNameLength = userNameField.val().length;
    // console.log(userNameLength);
    if(userNameLength < 2) {
      userNameField.addClass('__error');
    } else {
      userNameField.removeClass('__error');
    }
  });

  /* callback form submit function */

  // test calendar js

  var formUserDateField = $('.j-user-date'),
      formUserTimeField = $('.j-user-time');

  if(formUserDateField.length > 0) {
    flatpickr(formUserDateField, {
      minDate: "today",
      "locale": "ru",
      firstDayOfWeek: 2,
    });
  }
  if(formUserTimeField.length > 0) {
    flatpickr(formUserTimeField, {
      "locale": "ru",
      time_24hr: true,
      defaultHour: 9,
      enableTime: true,
      noCalendar: true,
    });
  }
  
  // test calendar js

  $('.j-callback-form').submit(function(e){
    e.preventDefault();
    var form = $(this),
        formUserNameField = form.find('.j-user-name-field'),
        formUserNameFieldVal = formUserNameField.val(),
        formUserPhoneField = form.find('.j-user-phone'),
        formUserPhoneFieldVal = formUserPhoneField.val(),
        formUserAddressField = form.find('.j-user-address'),
        formUserAddressFieldVal = formUserAddressField.val(),
        // formUserDateField = form.find('.j-user-date'),
        formUserDateFieldVal = form.find('.j-user-date').val(),
        // formUserTimeField = form.find('.j-user-time'),
        formUserTimeFieldVal = form.find('.j-user-time').val(),
        formUserCommentField = form.find('.j-user-comment'),
        formUserCommentFieldVal = formUserCommentField.val(),
        errorsArr = [];
    
        formUserTimeFieldVal = formUserTimeFieldVal.replace(':','-') + '-00';

    if(formUserNameFieldVal.length < 2) {
      formUserNameField.addClass('__error');
      errorsArr.push('1');
    } else {
      formUserNameField.removeClass('__error');
    }

    if (formUserPhoneFieldVal.length < 15) {
      formUserPhoneField.addClass('__error');
      errorsArr.push('1');
    } else {
      formUserPhoneField.removeClass('__error');
    }

    if (formUserAddressFieldVal.length < 3) {
      formUserAddressField.addClass('__error');
      errorsArr.push('1');
    } else {
      formUserAddressField.removeClass('__error');
    }

    if (formUserDateFieldVal.length < 9) {
      formUserDateField.addClass('__error');
      errorsArr.push('1');
    } else {
      formUserDateField.removeClass('__error');
    }

    if (formUserTimeFieldVal.length < 5) {
      formUserTimeField.addClass('__error');
      errorsArr.push('1');
    } else {
      formUserTimeField.removeClass('__error');
    }

    if(errorsArr.length === 0) {
      // console.log('start sending...');
      var checkboxInputsList = $('.j-checkbox__input'),
          chosenServicesList = [],
          chosenPropertyType = $('.j-property').attr('data-count'),
          chosenCleanType = $('.j-cleaning').attr('data-count'),
          chosenAllergyChecked = String($('.j-allergy-check').is(':checked'));
      
      checkboxInputsList.each(function(){
        var checkboxInputItem = $(this),
            checkboxInputItemId = checkboxInputItem.attr('id'),
            checkboxInputItemField = checkboxInputItem.closest('.j-checkbox-option').find('.j-checkbox__counter'),
            checkboxInputItemFieldVal = checkboxInputItemField.val(),
            checkboxInputItemChecked = checkboxInputItem.is(':checked');
        if(checkboxInputItemChecked === true) {
          chosenServicesList.push({
            itemId: checkboxInputItemId,
            itemValue: checkboxInputItemFieldVal
          });
        }
      });
      
      // console.log('chosenServicesList: ');
      // console.log(typeof(chosenServicesList));
      
      var resultJson = JSON.stringify({
        "services": chosenServicesList,
        "property": chosenPropertyType,
        "service": chosenCleanType,
        "is_allergy": chosenAllergyChecked,
        "name": formUserNameFieldVal,
        "phone": formUserPhoneFieldVal,
        "address": formUserAddressFieldVal,
        "datetime": formUserDateFieldVal+':'+formUserTimeFieldVal,
        "comment": formUserCommentFieldVal
      });

      // console.log(JSON.parse(resultJson));

      $.ajax({
        url: 'https://8055.ru/api/order',
        method: 'post',
        dataType: 'json',
        data: resultJson,
        success: function(data){
          // console.log(data);
          if(data.message === 'success') {
            dataLayer.push({'event': 'zakazaluborku'});
            showOverlay(overlay);
            formsHolder.fadeIn();
            setTimeout(function(){
              successModal.fadeIn();
            },300);
            setTimeout(function(){
              hideModals();
              form.trigger('reset');
            },1200);
          }
        }
      }); 
    } else {
      console.log('send errors');
    }
  });
});